amath-ng
========

A command line calculator for elementary and algebraic use, and more to come.

BUILDING
--------

Required libraries:

Boost.Random

Boost.Thread

Boost.System

GMP C

This project uses the C++14 standard.

Build by running 'make all'

Install by running 'make install'
